/* used to find the hosts */
SELECT f.id, f.last_name, MAX(gi.`date`), COUNT(DISTINCT gi.id)
FROM family f
	LEFT OUTER JOIN group_instance gi ON f.id = gi.host_family_id
WHERE f.active = 1
	AND f.`host` = 1
GROUP BY f.id, f.last_name#
ORDER BY MAX(gi.`date`) ASC, COUNT(DISTINCT gi.id) ASC
;


/* Use this to find group members for a host */
SELECT f.id, f.last_name, '1970-01-01' AS last_seen, 0 AS occurrences
FROM family f
WHERE f.id not in (
		SELECT DISTINCT f.id
		FROM family f INNER JOIN group_instance_members gim ON f.id = gim.family_id
		WHERE gim.group_instance_id in (
			SELECT DISTINCT group_instance_id
			FROM group_instance_members
			WHERE family_id = 1))
GROUP BY f.id
UNION
SELECT f2.id, f2.last_name, MAX(gi.`date`) AS last_seen, COUNT(DISTINCT gi.id) AS occurrences
FROM family f1
		INNER JOIN group_instance_members gim1 ON f1.id = gim1.family_id
		INNER JOIN (group_instance gi 
		INNER JOIN group_instance_members gim2 ON gi.id = gim2.group_instance_id
		INNER JOIN family f2 ON gim2.family_id = f2.id) ON gim1.group_instance_id = gi.id
WHERE (gi.occurred is null OR gi.occurred = 1) 
	AND f1.id != f2.id
	AND f1.id IN (1)
GROUP BY f2.id, f2.last_name
ORDER BY last_seen ASC, occurrences ASC
;

SELECT * 
FROM family f 
	LEFT OUTER JOIN group_instance_members gi ON f.id = gi.family_id 
WHERE active = 1 
	AND family_id is null;

SELECT group_instance_id, count(1) as groups, sum(members) as person_count
FROM group_instance_members gi
	INNER JOIN family f ON gi.family_id = f.id
GROUP BY group_instance_id;

SELECT group_instance_id, email_address, f.id, f.last_name, host_family_id
FROM group_instance_members gim
	INNER JOIN family f ON gim.family_id = f.id
	INNER JOIN group_instance gi ON gim.group_instance_id = gi.id
WHERE gi.`date` > NOW();

select gim.group_instance_id, f.id, f.last_name /*count(1), sum(members)*/
from family f
	left outer join group_instance_members gim on f.id = gim.family_id
where active = 1
	and f.id not in (
		select f.id 
		from family f 
			inner join group_instance_members gim on f.id = gim.family_id 
			inner join group_instance gi on gim.group_instance_id = gi.id
		where gi.`date` = '2015-03-01')
order by gim.group_instance_id;