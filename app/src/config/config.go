package config

import (
	"io/ioutil"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gomail.v1"
	yaml "gopkg.in/yaml.v2"
)

var appConfig AppConfig

type customVars map[string]string

type AppConfig struct {
	Db         DbConfig
	Email      EmailConfig
	CustomVars customVars
}

type user struct {
	Name     string
	Password string
}

type DbConfig struct {
	Dialect          string
	ConnectionString string
}

type EmailConfig struct {
	SmtpServer string
	Port       uint16
	User       user
}

func ReadConfig(filepath string) {
	configFile, err := ioutil.ReadFile(filepath)
	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(configFile, &appConfig)
	if err != nil {
		panic(err)
	}

	return
}

func GetDbVars() DbConfig {
	return appConfig.Db
}

func GetMailer() *gomail.Mailer {
	e := appConfig.Email
	return gomail.NewMailer(e.SmtpServer, e.User.Name, e.User.Password, int(e.Port))
}

func GetCustomVars() customVars {
	return appConfig.CustomVars
}
