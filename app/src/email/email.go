package email

import (
	"net/smtp"
	"strconv"
)

type EmailUser struct {
	Username    string
	Password    string
	EmailServer string
	Port        int
}

func GmailUserFactory(username string, password string) *EmailUser {
	return &EmailUser{username, password, "smtp.gmail.com", 587}
}

func (u *EmailUser) GetAuth() smtp.Auth {
	return smtp.PlainAuth("",
		u.Username,
		u.Password,
		u.EmailServer)
}

func (u *EmailUser) SendMail(to []string, msg []byte) error {
	return smtp.SendMail(
		u.EmailServer+":"+strconv.Itoa(u.Port),
		u.GetAuth(),
		u.Username,
		to,
		msg)
}
