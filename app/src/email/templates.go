package email

import (
	"github.com/hoisie/mustache"
)

type EmailData interface{}

func GenerateEmail(templateFileName string, data EmailData) string {
	return mustache.RenderFile(templateFileName, data)
}
