package repos

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"model"
)

type Host struct {
	Family     model.Family
	LastHosted time.Time
}

type Repository interface {
	GetPeopleCounts() (familyCount int, individualCount int)
	//GetHosts() []struct{family model.Family, lastHosted time.Time}
	GetMonthsGroups(meetingDate string) []model.GroupDetails
}

type DatabaseRepository struct {
	Db *sql.DB
}

func GetNewDatabaseRepository(dialect string, connString string) DatabaseRepository {
	db, err := sql.Open(dialect, connString)
	if err != nil {
		panic(err)
	}

	return DatabaseRepository{Db: db}
}

func (r *DatabaseRepository) GetPeopleCounts() (familyCount int, individualCount int) {
	err := r.Db.QueryRow("SELECT count(1), sum(members) FROM family WHERE active=1").Scan(&familyCount, &individualCount)
	if err != nil {
		panic(err)
	}
	return
}

/*
func (r *DatabaseRepository) GetHosts() []struct{family model.Family, lastHosted time.Time} {

}
*/
func (r *DatabaseRepository) GetMonthsGroups(meetingDate string) []model.GroupDetails {
	tmp := model.GroupInstance{}
	groupsRows, getGroupsErr := r.Db.Query(fmt.Sprintf("SELECT %s FROM group_instance WHERE `date` = '%s'", tmp.StringParams(), meetingDate))
	if getGroupsErr != nil {
		panic(getGroupsErr)
	}
	defer groupsRows.Close()

	groups := []model.GroupDetails{}

	for groupsRows.Next() {
		group := model.GroupDetails{}

		rowErr := groupsRows.Scan(group.GroupInstance.GetParams())
		if rowErr != nil {
			panic(rowErr)
		}

		// get host family details
		hostErr := r.Db.QueryRow(fmt.Sprintf("SELECT %s FROM family WHERE id = %d", group.GroupInstance.HostFamily.StringParams(), group.GroupInstance.HostFamily.Id)).Scan(group.GroupInstance.HostFamily.GetParams())
		if hostErr != nil {
			panic(hostErr)
		}

		// get group members
		membersRows, membersErr := r.Db.Query(fmt.Sprintf("SELECT %s FROM group_instance_members gi INNER JOIN family f ON gi.family_id = f.id WHERE group_instance_id = %d", "f."+group.GroupInstance.HostFamily.StringParams(), group.GroupInstance.Id))
		if membersErr != nil {
			panic(membersErr)
		}
		defer membersRows.Close()

		for membersRows.Next() {
			family := model.Family{}
			memberErr := membersRows.Scan(family.GetParams())
			if memberErr != nil {
				panic(memberErr)
			}

			group.Members = append(group.Members, family)
		}

		groups = append(groups, group)
	}

	return groups
}
