package main

import (
	"fmt"
	"strconv"
	"strings"

	"config"
	"email"
	"gopkg.in/gomail.v1"
	"model"
	"repos"
)

const meetingDate = "2015-04-01"

type ViewModel struct {
	From           string
	To             []string
	To_joined      string
	Month          string
	Year           int
	Host_last_name string
	Extra_text     string
	People         [](struct {
		First_names string
		Last_name   string
		Phone       string
	})
}

func formatPhoneNumber(phoneNum uint64) string {
	phoneStr := strconv.FormatUint(phoneNum, 10)
	return fmt.Sprintf("%v-%v-%v", phoneStr[0:3], phoneStr[3:6], phoneStr[6:])
}

func formatHostName(host model.Family) string {
	if host.MemberCount > 1 {
		result := "The <b>"
		if host.LastName[len(host.LastName)-1:] == "s" {
			result += host.LastName
		} else {
			result += host.LastName + "s"
		}
		return result + "</b> are"
	} else {
		return "<b>" + host.FirstName1 + " " + host.LastName + "</b> is"
	}
}

func main() {
	config.ReadConfig("config.yaml")

	dbConfig := config.GetDbVars()
	repo := repos.GetNewDatabaseRepository(dbConfig.Dialect, dbConfig.ConnectionString)
	defer repo.Db.Close()
	gmailer := config.GetMailer()
	customVars := config.GetCustomVars()

	// familyCount, individualCount := repo.GetPeopleCounts()

	for _, group := range repo.GetMonthsGroups(meetingDate) {

		templateData := ViewModel{From: customVars["emailFrom"], Month: group.GroupInstance.Date.Month().String(), Year: group.GroupInstance.Date.Year(), Host_last_name: formatHostName(group.GroupInstance.HostFamily), Extra_text: ""}

		for _, family := range group.Members {
			templateData.To = append(templateData.To, family.EmailAddress)
			var person struct {
				First_names string
				Last_name   string
				Phone       string
			}
			person.Last_name = family.LastName
			if family.CommPref == "phone" {
				person.Phone = " - " + formatPhoneNumber(family.PhoneNumber)
			}
			person.First_names = family.FirstName1
			if family.FirstName2.Valid {
				person.First_names += " and " + family.FirstName2.String
			}

			templateData.People = append(templateData.People, person)
		}
		templateData.To_joined = strings.Join(templateData.To, ",")

		/*	for testing
			templateData.To = []string{"brightam1@gmail.com"}
			templateData.To_joined = strings.Join(templateData.To, ",")
		*/
		msg := gomail.NewMessage()
		msg.SetHeader("From", templateData.From)
		msg.SetHeader("To", templateData.To...)
		msg.SetHeader("Subject", fmt.Sprintf("[%v] %v %v", "Saltshakers", templateData.Month, templateData.Year))
		msg.SetBody("text/html", email.GenerateEmail("/home/adam/workspace/saltshakers/email_templates/group.mustache", templateData))

		error := gmailer.Send(msg)
		if error != nil {
			panic(error)
		}

	}
	return
}
