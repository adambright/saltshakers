package model

import (
	"time"
)

type getter func() interface{}

type GroupInstance struct {
	Id         uint32
	HostFamily Family
	Date       time.Time
	EmailSent  sqlBool
}

func (gi *GroupInstance) StringParams() string {
	return "id, host_family_id, `date`, email_sent"
}

func (gi *GroupInstance) GetParams() (id *uint32, family *uint32, date *time.Time, emailSent *sqlBool) {
	id = &gi.Id
	family = &gi.HostFamily.Id
	date = &gi.Date
	emailSent = &gi.EmailSent
	return
}

type GroupDetails struct {
	GroupInstance GroupInstance
	Members       []Family
}
