package model

import (
	"database/sql"
)

type sqlBool []byte

type Family struct {
	Id             uint32
	LastName       string
	EmailAddress   string
	PhoneNumber    uint64
	CommPref       string
	Active         sqlBool
	Host           sqlBool
	FirstName1     string
	FirstName2     sql.NullString
	MemberCount    uint8
	WelcomeMsgSent sqlBool
}

func (f *Family) StringParams() string {
	return "id, last_name, email_address, phone_number, communication_preference, active, host, first_name_1, first_name_2, members, welcome_msg_sent"
}

func (f *Family) GetParams() (id *uint32, lastName *string, emailAddress *string, phoneNumber *uint64, commPref *string, active *sqlBool, host *sqlBool, firstName1 *string, firstName2 *sql.NullString, memberCount *uint8, welcomeMsgSent *sqlBool) {
	id = &f.Id
	lastName = &f.LastName
	emailAddress = &f.EmailAddress
	phoneNumber = &f.PhoneNumber
	commPref = &f.CommPref
	active = &f.Active
	host = &f.Host
	firstName1 = &f.FirstName1
	firstName2 = &f.FirstName2
	memberCount = &f.MemberCount
	welcomeMsgSent = &f.WelcomeMsgSent
	return
}
